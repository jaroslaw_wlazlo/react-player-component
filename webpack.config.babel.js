import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';

const config = {
  devtool: 'cheap-module-source-map',
  entry: {
    bundle: `${__dirname}/demo.js`,
  },
  module: {
    rules: [{
      test: /\.(js)?$/,
      loader: 'babel-loader',
      exclude: /node_modules/,
    }, {
      test: /\.(ejs)?$/,
      exclude: /node_modules/,
      loader: 'ejs-render-loader',
    }, {
      test: /\.*css$/,
      use: ['style-loader', 'css-loader', 'sass-loader'],
    }, {
      test: /\.(ttf)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
      loader: 'file-loader',
    }, {
      test: /\.svg/,
      loader: 'file-loader?mimetype=image/svg+xml',
    }, {
      test: /\.woff/,
      loader: 'file-loader?mimetype=application/font-woff',
    }, {
      test: /\.woff2/,
      loader: 'file-loader?mimetype=application/font-woff',
    }, {
      test: /\.ttf/,
      loader: 'file-loader?mimetype=application/octet-stream',
    }, {
      test: /\.eot/,
      loader: 'file-loader',
    }, {
      test: /\.otf/,
      loader: 'file-loader',
    }],
  },
  output: {
    path: `${__dirname}/demo`,
    publicPath: 'http://localhost:3002/',
    filename: 'demo.js',
  },
  resolve: {
    extensions: ['.js'],
  },
  devServer: {
    contentBase: `${__dirname}/demo`,
    hot: true,
    host: 'localhost',
    port: 3002,
    inline: true,
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: `${__dirname}/demo.ejs`,
    }),
    new webpack.HotModuleReplacementPlugin(),
  ],
};

export default config;
