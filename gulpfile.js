const gulp = require('gulp');

gulp.task('styles-compile', function() {
  return gulp
    .src('./src/**/*.scss')
    .pipe(gulp.dest('./build'));
});

