'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  trackUrl: _propTypes2.default.string.isRequired,
  trackTitle: _propTypes2.default.string.isRequired,
  coverUrl: _propTypes2.default.string.isRequired,
  artistName: _propTypes2.default.string.isRequired,
  rate: _propTypes2.default.number,
  rateCount: _propTypes2.default.number
};