'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _track = require('./track');

Object.defineProperty(exports, 'track', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_track).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }