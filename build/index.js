'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _PlayerComponent = require('./PlayerComponent');

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_PlayerComponent).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }