'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _withProgressBar = require('../../hoc/withProgressBar');

var _withProgressBar2 = _interopRequireDefault(_withProgressBar);

var _BEM = require('../../utils/BEM');

var _BEM2 = _interopRequireDefault(_BEM);

require('./TimeBar.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var b = new _BEM2.default('time-bar');

var TimeBar = function (_React$PureComponent) {
  _inherits(TimeBar, _React$PureComponent);

  function TimeBar() {
    _classCallCheck(this, TimeBar);

    return _possibleConstructorReturn(this, (TimeBar.__proto__ || Object.getPrototypeOf(TimeBar)).apply(this, arguments));
  }

  _createClass(TimeBar, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        {
          className: b.withMod(this.props.theme),
          onMouseLeave: this.props.unsetMouseHold,
          onMouseUp: this.props.unsetMouseHold
        },
        _react2.default.createElement(
          'div',
          { className: b.el('label').withMod('min') },
          this.props.labelMin
        ),
        _react2.default.createElement(
          'div',
          {
            ref: this.props.progressWrapperRef,
            className: b.el('line'),
            onClick: this.props.onBarClick,
            onMouseMove: this.props.onBarMouseMove
          },
          _react2.default.createElement('div', {
            className: b.el('indicator'),
            onMouseDown: this.props.setMouseHold,
            onMouseUp: this.props.unsetMouseHold,
            onClick: this.props.onIndicatorClick,
            style: {
              left: this.props.indicatorPercentagePosition - 5 + '%' // make it display in the middle of pointer
            }
          })
        ),
        _react2.default.createElement(
          'div',
          { className: b.el('label').withMod('max') },
          this.props.labelMax
        )
      );
    }
  }]);

  return TimeBar;
}(_react2.default.PureComponent);

TimeBar.propTypes = {
  progressWrapperRef: _propTypes2.default.func.isRequired,
  onBarClick: _propTypes2.default.func.isRequired,
  onBarMouseMove: _propTypes2.default.func.isRequired,
  setMouseHold: _propTypes2.default.func.isRequired,
  unsetMouseHold: _propTypes2.default.func.isRequired,
  onIndicatorClick: _propTypes2.default.func.isRequired,
  indicatorPercentagePosition: _propTypes2.default.number.isRequired,
  labelMin: _propTypes2.default.string.isRequired,
  labelMax: _propTypes2.default.string.isRequired,
  theme: _propTypes2.default.string.isRequired
};
exports.default = (0, _withProgressBar2.default)(TimeBar);