'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _TimeBar = require('../TimeBar');

var _TimeBar2 = _interopRequireDefault(_TimeBar);

var _utils = require('../../utils');

require('./Track.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var b = new _utils.BEM('track');

var Track = function (_React$Component) {
  _inherits(Track, _React$Component);

  function Track() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Track);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Track.__proto__ || Object.getPrototypeOf(Track)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      trackDuration: 0,
      currentTrackTime: 0
    }, _this.onAudioDataLoaded = function (_ref2) {
      var _ref2$target = _ref2.target,
          duration = _ref2$target.duration,
          currentTime = _ref2$target.currentTime;

      _this.setState({
        trackDuration: Math.floor(duration),
        currentTrackTime: currentTime
      });
      if (_this.props.shouldPlayTrack) {
        _this.audioDOMElement.play();
        _this.props.onTrackPlay(_this.props.trackData);
      }
    }, _this.onAudioTrackEnd = function () {
      _this.audioDOMElement.currentTime = 0;
      _this.props.onTrackEnd();
    }, _this.onAudioTimeUpdate = function (e) {
      var currentTrackTime = _this.state.currentTrackTime;

      var nextCurrentTime = Math.floor(e.target.currentTime);

      if (nextCurrentTime !== currentTrackTime) {
        _this.setState({
          currentTrackTime: nextCurrentTime
        });
      }
    }, _this.changeCurrentTrackTime = function (percentage) {
      _this.audioDOMElement.currentTime = Math.floor(percentage / 100 * _this.state.trackDuration);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Track, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.audioDOMElement = document.createElement('audio');
      this.audioDOMElement.src = this.props.trackData.trackUrl;
      this.audioDOMElement.volume = this.props.volume;
      this.audioDOMElement.addEventListener('loadeddata', this.onAudioDataLoaded);
      this.audioDOMElement.addEventListener('timeupdate', this.onAudioTimeUpdate);
      this.audioDOMElement.addEventListener('ended', this.onAudioTrackEnd);
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(_ref3) {
      var shouldPlayTrack = _ref3.shouldPlayTrack,
          trackData = _ref3.trackData,
          volume = _ref3.volume;

      var isTrackPaused = this.audioDOMElement.paused;

      if (this.audioDOMElement.src !== trackData.trackUrl) {
        this.audioDOMElement.src = trackData.trackUrl;
      }

      if (shouldPlayTrack && isTrackPaused) {
        this.audioDOMElement.play();
        this.props.onTrackPlay(this.props.trackData);
      } else if (!shouldPlayTrack && !isTrackPaused) {
        this.audioDOMElement.pause();
      }

      if (volume !== this.props.volume) {
        this.audioDOMElement.volume = volume;
      }
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.audioDOMElement.removeEventListener('loadeddata', this.onAudioDataLoaded);
      this.audioDOMElement.removeEventListener('timeupdate', this.onAudioTimeUpdate);
      this.audioDOMElement.removeEventListener('ended', this.onAudioTrackEnd);
      this.audioDOMElement.pause();
    }
  }, {
    key: 'render',
    value: function render() {
      var _state = this.state,
          currentTrackTime = _state.currentTrackTime,
          trackDuration = _state.trackDuration;

      return _react2.default.createElement(
        'div',
        { className: b },
        _react2.default.createElement(
          'div',
          { className: b.el('cover') },
          _react2.default.createElement('img', { className: b.el('cover-image'), src: this.props.trackData.coverUrl, alt: '' })
        ),
        _react2.default.createElement(_TimeBar2.default, {
          current: currentTrackTime,
          max: trackDuration,
          labelMin: _utils.timeUtils.convertSecondsToHumanTime(currentTrackTime),
          labelMax: _utils.timeUtils.convertSecondsToHumanTime(trackDuration),
          theme: this.props.theme,
          onChange: this.changeCurrentTrackTime
        })
      );
    }
  }]);

  return Track;
}(_react2.default.Component);

Track.defaultProps = {
  shouldPlayTrack: false,
  volume: 1
};
Track.propTypes = {
  theme: _propTypes2.default.string.isRequired,
  trackData: _propTypes2.default.shape({
    trackUrl: _propTypes2.default.string.isRequired,
    trackTitle: _propTypes2.default.string,
    coverUrl: _propTypes2.default.string,
    artistName: _propTypes2.default.string
  }).isRequired,
  shouldPlayTrack: _propTypes2.default.bool,
  onTrackEnd: _propTypes2.default.func.isRequired,
  onTrackPlay: _propTypes2.default.func.isRequired,
  volume: _propTypes2.default.number
};
exports.default = Track;