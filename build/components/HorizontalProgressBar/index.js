'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _HorizontalProgressBar = require('./HorizontalProgressBar');

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_HorizontalProgressBar).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }