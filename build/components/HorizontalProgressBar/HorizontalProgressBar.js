'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _withProgressBar = require('../../hoc/withProgressBar');

var _withProgressBar2 = _interopRequireDefault(_withProgressBar);

require('./HorizontalProgressBar.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var HorizontalProgressBar = function (_React$PureComponent) {
  _inherits(HorizontalProgressBar, _React$PureComponent);

  function HorizontalProgressBar() {
    _classCallCheck(this, HorizontalProgressBar);

    return _possibleConstructorReturn(this, (HorizontalProgressBar.__proto__ || Object.getPrototypeOf(HorizontalProgressBar)).apply(this, arguments));
  }

  _createClass(HorizontalProgressBar, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        {
          className: (0, _classnames2.default)('progress-bar', 'progress-bar--' + this.props.theme),
          onMouseLeave: this.props.unsetMouseHold,
          onMouseUp: this.props.unsetMouseHold
        },
        _react2.default.createElement(
          'div',
          { className: 'progress-bar__label progress-bar__label--min' },
          this.props.labelMin
        ),
        _react2.default.createElement(
          'div',
          {
            ref: this.props.progressWrapperRef,
            className: 'progress-bar__progress-line',
            onClick: this.props.onBarClick,
            onMouseMove: this.props.onBarMouseMove
          },
          _react2.default.createElement('div', {
            className: 'progress-bar__progress-indicator',
            onMouseDown: this.props.setMouseHold,
            onMouseUp: this.props.unsetMouseHold,
            onClick: this.props.onIndicatorClick,
            style: {
              left: this.props.indicatorPercentagePosition - 5 + '%' // make it display in the middle of pointer
            }
          })
        ),
        _react2.default.createElement(
          'div',
          { className: 'progress-bar__label progress-bar__label--max' },
          this.props.labelMax
        )
      );
    }
  }]);

  return HorizontalProgressBar;
}(_react2.default.PureComponent);

HorizontalProgressBar.propTypes = {
  progressWrapperRef: _propTypes2.default.func.isRequired,
  onBarClick: _propTypes2.default.func.isRequired,
  onBarMouseMove: _propTypes2.default.func.isRequired,
  setMouseHold: _propTypes2.default.func.isRequired,
  unsetMouseHold: _propTypes2.default.func.isRequired,
  onIndicatorClick: _propTypes2.default.func.isRequired,
  indicatorPercentagePosition: _propTypes2.default.number.isRequired,
  labelMin: _propTypes2.default.string.isRequired,
  labelMax: _propTypes2.default.string.isRequired,
  theme: _propTypes2.default.string.isRequired
};
exports.default = (0, _withProgressBar2.default)(HorizontalProgressBar);