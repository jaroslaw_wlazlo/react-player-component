'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _shapes = require('../../shapes');

var _PlayerNavigationHeading = require('./components/PlayerNavigationHeading');

var _PlayerNavigationHeading2 = _interopRequireDefault(_PlayerNavigationHeading);

var _PlayerNavigationMainSection = require('./components/PlayerNavigationMainSection');

var _PlayerNavigationMainSection2 = _interopRequireDefault(_PlayerNavigationMainSection);

var _PlayerNavigationFooter = require('./components/PlayerNavigationFooter');

var _PlayerNavigationFooter2 = _interopRequireDefault(_PlayerNavigationFooter);

require('./PlayerNavigation.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

var PlayerNavigation = function PlayerNavigation(_ref) {
  var theme = _ref.theme,
      props = _objectWithoutProperties(_ref, ['theme']);

  return _react2.default.createElement(
    'div',
    { className: 'rpc-player-navigation' },
    _react2.default.createElement(_PlayerNavigationHeading2.default, {
      theme: theme,
      isTrackPlayed: props.isTrackPlayed,
      isFavouritesActive: props.isFavouritesActive,
      isPlayerLooped: props.isPlayerLooped,
      onLoopButtonClick: props.onLoopButtonClick
    }),
    _react2.default.createElement(_PlayerNavigationMainSection2.default, {
      theme: theme,
      trackData: props.trackData,
      onRateStarClick: props.onRateStarClick,
      isRatingDisabled: props.isRatingDisabled
    }),
    _react2.default.createElement(_PlayerNavigationFooter2.default, {
      theme: theme,
      playButtonIcon: props.isTrackPlayed ? 'pause' : 'play',
      onPlayButtonClick: props.onPlayButtonClick,
      onForwardButtonClick: props.onForwardButtonClick,
      onBackwardButtonClick: props.onBackwardButtonClick,
      onShuffleButtonClick: props.onShuffleButtonClick,
      onVolumeBarChange: props.onVolumeBarChange,
      isPlayerShuffled: props.isPlayerShuffled,
      playerVolume: props.playerVolume
    })
  );
};

PlayerNavigation.propTypes = {
  theme: _propTypes2.default.string.isRequired,
  isFavouritesActive: _propTypes2.default.bool,
  isPlayerLooped: _propTypes2.default.bool,
  isPlayerShuffled: _propTypes2.default.bool,
  isTrackPlayed: _propTypes2.default.bool,
  onPlayButtonClick: _propTypes2.default.func.isRequired,
  onForwardButtonClick: _propTypes2.default.func.isRequired,
  onBackwardButtonClick: _propTypes2.default.func.isRequired,
  onShuffleButtonClick: _propTypes2.default.func.isRequired,
  onLoopButtonClick: _propTypes2.default.func.isRequired,
  trackData: _propTypes2.default.shape(_shapes.track).isRequired,
  onRateStarClick: _propTypes2.default.func.isRequired,
  onVolumeBarChange: _propTypes2.default.func.isRequired,
  isRatingDisabled: _propTypes2.default.bool,
  playerVolume: _propTypes2.default.number.isRequired
};

PlayerNavigation.defaultProps = {
  isFavouritesActive: false,
  isPlayerLooped: false,
  isPlayerShuffled: false,
  isTrackPlayed: false,
  isRatingDisabled: false
};

exports.default = PlayerNavigation;