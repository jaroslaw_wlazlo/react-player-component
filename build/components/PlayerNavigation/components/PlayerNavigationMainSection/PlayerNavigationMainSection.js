'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _RatingStars = require('./components/RatingStars');

var _RatingStars2 = _interopRequireDefault(_RatingStars);

var _shapes = require('../../../../shapes');

var _BEM = require('../../../../utils/BEM');

var _BEM2 = _interopRequireDefault(_BEM);

require('./PlayerNavigationMainSection.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var b = new _BEM2.default('player-navigation-main-section');

var PlayerNavigationMainSection = function PlayerNavigationMainSection(_ref) {
  var theme = _ref.theme,
      trackData = _ref.trackData,
      onRateStarClick = _ref.onRateStarClick,
      isRatingDisabled = _ref.isRatingDisabled;
  return _react2.default.createElement(
    'div',
    { className: b.withMod(theme) },
    _react2.default.createElement(
      'h3',
      { className: b.el('artist-name') },
      trackData.artistName
    ),
    _react2.default.createElement(
      'h4',
      { className: b.el('song-title') },
      trackData.trackTitle
    ),
    !isRatingDisabled && _react2.default.createElement(
      'div',
      { className: b.el('rating') },
      _react2.default.createElement(_RatingStars2.default, {
        rate: trackData.rate,
        rateCount: trackData.rateCount,
        onStarClick: function onStarClick(rate) {
          return onRateStarClick(rate, trackData);
        }
      })
    )
  );
};

PlayerNavigationMainSection.propTypes = {
  trackData: _propTypes2.default.shape(_shapes.track).isRequired,
  theme: _propTypes2.default.string.isRequired,
  onRateStarClick: _propTypes2.default.func.isRequired,
  isRatingDisabled: _propTypes2.default.bool.isRequired
};

exports.default = PlayerNavigationMainSection;