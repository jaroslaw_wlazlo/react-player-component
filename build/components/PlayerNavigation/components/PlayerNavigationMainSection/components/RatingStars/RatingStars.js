'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _FontIcon = require('../../../../../FontIcon');

var _FontIcon2 = _interopRequireDefault(_FontIcon);

var _math = require('../../../../../../utils/math');

var _BEM = require('../../../../../../utils/BEM');

var _BEM2 = _interopRequireDefault(_BEM);

require('./RatingStars.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var b = new _BEM2.default('rating-stars');

var RATE_STEP = 0.5;

var RatingStar = function (_React$Component) {
  _inherits(RatingStar, _React$Component);

  function RatingStar() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, RatingStar);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = RatingStar.__proto__ || Object.getPrototypeOf(RatingStar)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      hoveredStarIndex: null
    }, _this.getStarIconByIndex = function (index) {
      var hoveredStarIndex = _this.state.hoveredStarIndex;

      var isAnyStarHovered = hoveredStarIndex !== null;

      if (isAnyStarHovered) {
        return index <= hoveredStarIndex ? 'star' : 'star-o';
      }

      var roundedRate = (0, _math.roundToNearestHalf)(_this.props.rate);
      if (roundedRate === index + RATE_STEP) {
        return 'star-half-o';
      } else if (roundedRate >= index + 1) {
        return 'star';
      }

      return 'star-o';
    }, _this.starOnMouseLeave = function (e) {
      e.stopPropagation();
      _this.setState({
        hoveredStarIndex: null
      });
    }, _this.createStarOnMouseOver = function (index) {
      return function (e) {
        e.stopPropagation();
        _this.setState({
          hoveredStarIndex: index
        });
      };
    }, _this.onFirstStarClick = function () {
      _this.props.onStarClick(1);
    }, _this.onSecondStarClick = function () {
      _this.props.onStarClick(2);
    }, _this.onThirdStarClick = function () {
      _this.props.onStarClick(3);
    }, _this.onFourthStarClick = function () {
      _this.props.onStarClick(4);
    }, _this.onFifthStarClick = function () {
      _this.props.onStarClick(5);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  // separate onClicks are created here to prevent recreating every render
  /* eslint-disable react/sort-comp */


  _createClass(RatingStar, [{
    key: 'render',

    /* eslint-disable react/sort-comp */

    value: function render() {
      var starClassName = b.el('star');
      return _react2.default.createElement(
        'div',
        { className: b },
        _react2.default.createElement(_FontIcon2.default, {
          className: starClassName,
          icon: this.getStarIconByIndex(0),
          onMouseOver: this.createStarOnMouseOver(0),
          onMouseLeave: this.starOnMouseLeave,
          onClick: this.onFirstStarClick
        }),
        _react2.default.createElement(_FontIcon2.default, {
          className: starClassName,
          icon: this.getStarIconByIndex(1),
          onMouseOver: this.createStarOnMouseOver(1),
          onMouseLeave: this.starOnMouseLeave,
          onClick: this.onSecondStarClick
        }),
        _react2.default.createElement(_FontIcon2.default, {
          className: starClassName,
          icon: this.getStarIconByIndex(2),
          onMouseOver: this.createStarOnMouseOver(2),
          onMouseLeave: this.starOnMouseLeave,
          onClick: this.onThirdStarClick
        }),
        _react2.default.createElement(_FontIcon2.default, {
          className: starClassName,
          icon: this.getStarIconByIndex(3),
          onMouseOver: this.createStarOnMouseOver(3),
          onMouseLeave: this.starOnMouseLeave,
          onClick: this.onFourthStarClick
        }),
        _react2.default.createElement(_FontIcon2.default, {
          className: starClassName,
          icon: this.getStarIconByIndex(4),
          onMouseOver: this.createStarOnMouseOver(4),
          onMouseLeave: this.starOnMouseLeave,
          onClick: this.onFifthStarClick
        }),
        _react2.default.createElement(
          'div',
          { className: b.el('count') },
          '( ',
          this.props.rateCount,
          ' )'
        )
      );
    }
  }]);

  return RatingStar;
}(_react2.default.Component);

RatingStar.propTypes = {
  rate: _propTypes2.default.number.isRequired,
  rateCount: _propTypes2.default.number.isRequired,
  onStarClick: _propTypes2.default.func.isRequired
};
exports.default = RatingStar;