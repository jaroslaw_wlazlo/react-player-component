'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _PlayerNavigationMainSection = require('./PlayerNavigationMainSection');

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_PlayerNavigationMainSection).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }