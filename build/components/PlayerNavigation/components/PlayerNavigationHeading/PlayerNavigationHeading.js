'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

var _FontIcon = require('../../../FontIcon');

var _FontIcon2 = _interopRequireDefault(_FontIcon);

var _BEM = require('../../../../utils/BEM');

var _BEM2 = _interopRequireDefault(_BEM);

require('./PlayerNavigationHeading.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var b = new _BEM2.default('player-navigation-heading');

var PlayerNavigationHeading = function (_React$Component) {
  _inherits(PlayerNavigationHeading, _React$Component);

  function PlayerNavigationHeading() {
    _classCallCheck(this, PlayerNavigationHeading);

    return _possibleConstructorReturn(this, (PlayerNavigationHeading.__proto__ || Object.getPrototypeOf(PlayerNavigationHeading)).apply(this, arguments));
  }

  _createClass(PlayerNavigationHeading, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: b.withMod(this.props.theme) },
        _react2.default.createElement(_FontIcon2.default, {
          className: b.el('icon').withMod({ active: this.props.isPlayerLooped }),
          icon: 'repeat',
          title: 'repeat',
          onClick: this.props.onLoopButtonClick
        }),
        _react2.default.createElement(
          'h2',
          { className: b.el('text') },
          this.props.isTrackPlayed ? 'odtwarzanie...' : 'odwtórz ten utwór'
        ),
        _react2.default.createElement(_FontIcon2.default, {
          className: b.el('icon').withMod({ active: this.props.isAddedToFavourites }),
          icon: 'heart',
          title: 'add to favourites',
          onClick: this.props.isFavouritesActive && this.props.toggleAddedToFavouritesActive
        })
      );
    }
  }]);

  return PlayerNavigationHeading;
}(_react2.default.Component);

PlayerNavigationHeading.propTypes = {
  isTrackPlayed: _propTypes2.default.bool,
  isPlayerLooped: _propTypes2.default.bool,
  isFavouritesActive: _propTypes2.default.bool,
  isAddedToFavourites: _propTypes2.default.bool,
  onLoopButtonClick: _propTypes2.default.func.isRequired,
  toggleAddedToFavouritesActive: _propTypes2.default.func,
  theme: _propTypes2.default.string.isRequired
};
PlayerNavigationHeading.defaultProps = {
  isAddedToFavourites: false,
  isPlayerLooped: false,
  isTrackPlayed: false,
  isFavouritesActive: false,
  toggleAddedToFavouritesActive: null
};
exports.default = PlayerNavigationHeading;