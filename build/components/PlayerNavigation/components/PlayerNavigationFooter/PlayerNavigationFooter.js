'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _FontIcon = require('../../../FontIcon');

var _FontIcon2 = _interopRequireDefault(_FontIcon);

var _VolumeBar = require('../../../VolumeBar');

var _VolumeBar2 = _interopRequireDefault(_VolumeBar);

var _BEM = require('../../../../utils/BEM');

var _BEM2 = _interopRequireDefault(_BEM);

require('./PlayerNavigationFooter.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var PLAYER_VOLUME_MAX = 1;

var b = new _BEM2.default('player-navigation-footer');

var PlayerNavigationFooter = function (_React$Component) {
  _inherits(PlayerNavigationFooter, _React$Component);

  function PlayerNavigationFooter() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, PlayerNavigationFooter);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = PlayerNavigationFooter.__proto__ || Object.getPrototypeOf(PlayerNavigationFooter)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      isVolumeBarActive: false
    }, _this.toggleVolumeBarActive = function () {
      _this.setState({
        isVolumeBarActive: !_this.state.isVolumeBarActive
      });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(PlayerNavigationFooter, [{
    key: 'getVolumeIcon',
    value: function getVolumeIcon() {
      var playerVolume = this.props.playerVolume;


      if (playerVolume === 0) {
        return 'volume-off';
      } else if (playerVolume <= PLAYER_VOLUME_MAX / 2 && playerVolume !== PLAYER_VOLUME_MAX - 0.05) {
        return 'volume-down';
      }

      return 'volume-up';
    }
  }, {
    key: 'render',
    value: function render() {
      var playerVolume = this.props.playerVolume;
      // let the volume icon show as active a little bit earlier, give it error border of 0.05

      return _react2.default.createElement(
        'div',
        { className: b.withMod(this.props.theme) },
        _react2.default.createElement(
          'div',
          { className: b.el('volume') },
          _react2.default.createElement(_FontIcon2.default, {
            className: b.el('icon').withMod({ active: playerVolume >= PLAYER_VOLUME_MAX - 0.05 }),
            icon: this.getVolumeIcon(),
            onClick: this.toggleVolumeBarActive
          }),
          this.state.isVolumeBarActive && _react2.default.createElement(_VolumeBar2.default, {
            current: playerVolume,
            max: 1,
            theme: this.props.theme,
            onChange: this.props.onVolumeBarChange
          })
        ),
        _react2.default.createElement(_FontIcon2.default, {
          className: b.el('icon').withMod('backward'),
          icon: 'fast-backward',
          onClick: this.props.onBackwardButtonClick
        }),
        _react2.default.createElement(_FontIcon2.default, {
          className: b.el('icon').withMod('active'),
          icon: this.props.playButtonIcon,
          onClick: this.props.onPlayButtonClick
        }),
        _react2.default.createElement(_FontIcon2.default, {
          className: b.el('icon').withMod('forward'),
          icon: 'fast-forward',
          onClick: this.props.onForwardButtonClick
        }),
        _react2.default.createElement(_FontIcon2.default, {
          className: b.el('icon').withMod({ active: this.props.isPlayerShuffled }),
          icon: 'random',
          onClick: this.props.onShuffleButtonClick
        })
      );
    }
  }]);

  return PlayerNavigationFooter;
}(_react2.default.Component);

PlayerNavigationFooter.propTypes = {
  theme: _propTypes2.default.string.isRequired,
  playButtonIcon: _propTypes2.default.string.isRequired,
  onPlayButtonClick: _propTypes2.default.func.isRequired,
  onForwardButtonClick: _propTypes2.default.func.isRequired,
  onBackwardButtonClick: _propTypes2.default.func.isRequired,
  onShuffleButtonClick: _propTypes2.default.func.isRequired,
  onVolumeBarChange: _propTypes2.default.func.isRequired,
  isPlayerShuffled: _propTypes2.default.bool.isRequired,
  playerVolume: _propTypes2.default.number.isRequired
};
exports.default = PlayerNavigationFooter;