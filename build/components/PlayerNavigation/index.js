'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _PlayerNavigation = require('./PlayerNavigation');

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_PlayerNavigation).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }