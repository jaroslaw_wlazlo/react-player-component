'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _TrackList = require('./TrackList');

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_TrackList).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }