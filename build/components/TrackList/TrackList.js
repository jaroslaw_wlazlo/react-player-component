'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _shapes = require('../../shapes');

var _BEM = require('../../utils/BEM');

var _BEM2 = _interopRequireDefault(_BEM);

require('./TrackList.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var b = new _BEM2.default('track-list');

var getTracks = function getTracks(tracksData, onItemClick) {
  return tracksData.map(function (track, i) {
    return _react2.default.createElement(
      'li',
      {
        key: track.trackTitle + '_' + track.rate + '_' + i,
        className: b.el('item'),
        onClick: function onClick() {
          return onItemClick(i);
        }
      },
      _react2.default.createElement(
        'span',
        { className: b.el('item-part') },
        track.trackTitle
      ),
      _react2.default.createElement(
        'span',
        { className: b.el('item-part') },
        track.artistName
      )
    );
  });
};

var TrackList = function TrackList(_ref) {
  var tracksData = _ref.tracksData,
      onItemClick = _ref.onItemClick,
      theme = _ref.theme;
  return _react2.default.createElement(
    'div',
    { className: b.withMod(theme) },
    _react2.default.createElement(
      'ul',
      { className: b.el('list') },
      getTracks(tracksData, onItemClick)
    )
  );
};

TrackList.propTypes = {
  tracksData: _propTypes2.default.arrayOf(_propTypes2.default.shape(_shapes.track)).isRequired,
  onItemClick: _propTypes2.default.func.isRequired,
  theme: _propTypes2.default.string.isRequired
};

exports.default = TrackList;