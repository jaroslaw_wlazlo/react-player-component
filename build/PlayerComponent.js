'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _shapes = require('./shapes');

var _Track = require('./components/Track');

var _Track2 = _interopRequireDefault(_Track);

var _PlayerNavigation = require('./components/PlayerNavigation');

var _PlayerNavigation2 = _interopRequireDefault(_PlayerNavigation);

var _TrackList = require('./components/TrackList');

var _TrackList2 = _interopRequireDefault(_TrackList);

var _math = require('./utils/math');

require('font-awesome/css/font-awesome.css');

require('./PlayerComponent.scss');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

// Want to keep style imports in one place
/* eslint-disable import/first */

/* eslint-disable import/first */


var PlayerComponent = function (_React$Component) {
  _inherits(PlayerComponent, _React$Component);

  function PlayerComponent() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, PlayerComponent);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = PlayerComponent.__proto__ || Object.getPrototypeOf(PlayerComponent)).call.apply(_ref, [this].concat(args))), _this), _this.state = {
      currentTrackIndex: 0,
      shouldLoop: false,
      isTrackPlayed: false,
      shouldShuffle: false,
      playerVolume: 1
    }, _this.onVolumeBarChange = function (percentage) {
      _this.setState({
        playerVolume: percentage / 100
      });
    }, _this.goToPreviousTrack = function () {
      var nextIndex = _this.getNextIndex();

      if (nextIndex === null) {
        var currentTrackIndex = _this.state.currentTrackIndex;

        nextIndex = currentTrackIndex > 0 ? currentTrackIndex - 1 : 0;
      }

      _this.changeTrack(nextIndex);
    }, _this.goToNextTrack = function () {
      var nextIndex = _this.getNextIndex();

      if (!nextIndex) {
        var currentTrackIndex = _this.state.currentTrackIndex;

        var lastTrackIndex = _this.props.tracks.length - 1;
        nextIndex = currentTrackIndex < lastTrackIndex ? currentTrackIndex + 1 : lastTrackIndex;
      }

      _this.changeTrack(nextIndex);
    }, _this.toggleTrackPlay = function () {
      _this.setState({
        isTrackPlayed: !_this.state.isTrackPlayed
      });
    }, _this.togglePlayerLoop = function () {
      var shouldLoop = _this.state.shouldLoop;

      var toSet = {
        shouldLoop: !shouldLoop
      };
      if (!shouldLoop) {
        toSet.shouldShuffle = false;
      }
      _this.setState(toSet);
    }, _this.togglePlayerShuffle = function () {
      var shouldShuffle = _this.state.shouldShuffle;

      var toSet = {
        shouldShuffle: !shouldShuffle
      };
      if (!shouldShuffle) {
        toSet.shouldLoop = false;
      }
      _this.setState(toSet);
    }, _this.changeTrack = function (trackIndex) {
      _this.setState({
        currentTrackIndex: trackIndex
      });
      _this.props.onTrackChange(_this.props.tracks[trackIndex]);
    }, _this.handleTrackEnd = function () {
      var nextIndex = _this.getNextIndex();
      if (nextIndex !== null) {
        _this.setState({
          currentTrackIndex: nextIndex
        });
      }
      _this.props.onTrackChange(_this.props.tracks[nextIndex]);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(PlayerComponent, [{
    key: 'componentWillMount',
    value: function componentWillMount() {
      this.setState({
        isTrackPlayed: this.props.autoPlay,
        currentTrackIndex: this.props.activeTrackIndex || 0
      });
    }
  }, {
    key: 'componentWillReceiveProps',
    value: function componentWillReceiveProps(_ref2) {
      var nextActiveTrackIndex = _ref2.activeTrackIndex;

      if (nextActiveTrackIndex && nextActiveTrackIndex !== this.state.currentTrackIndex) {
        this.changeTrack(nextActiveTrackIndex);
      }
    }
  }, {
    key: 'getNextIndex',
    value: function getNextIndex() {
      var _state = this.state,
          currentTrackIndex = _state.currentTrackIndex,
          shouldShuffle = _state.shouldShuffle,
          shouldLoop = _state.shouldLoop;

      var lastTrackIndex = this.props.tracks.length - 1;

      if (shouldLoop) {
        return currentTrackIndex;
      }

      if (shouldShuffle) {
        return (0, _math.getRandomIntInRange)(0, lastTrackIndex);
      }

      return null;
    }
  }, {
    key: 'render',
    value: function render() {
      var theme = this.props.theme;
      var _state2 = this.state,
          shouldLoop = _state2.shouldLoop,
          currentTrackIndex = _state2.currentTrackIndex,
          isTrackPlayed = _state2.isTrackPlayed;

      var currentTrack = this.props.tracks[currentTrackIndex];

      return _react2.default.createElement(
        'div',
        { className: 'rpc-player' },
        _react2.default.createElement(_Track2.default, {
          theme: theme,
          trackData: currentTrack,
          shouldPlayTrack: isTrackPlayed,
          onTrackEnd: this.handleTrackEnd,
          onTrackPlay: this.props.onTrackPlayed,
          volume: this.state.playerVolume
        }),
        _react2.default.createElement(_PlayerNavigation2.default, {
          theme: theme,
          isPlayerLooped: shouldLoop,
          isPlayerShuffled: this.state.shouldShuffle,
          trackData: currentTrack,
          onPlayButtonClick: this.toggleTrackPlay,
          onForwardButtonClick: this.goToNextTrack,
          onBackwardButtonClick: this.goToPreviousTrack,
          onShuffleButtonClick: this.togglePlayerShuffle,
          onLoopButtonClick: this.togglePlayerLoop,
          isTrackPlayed: isTrackPlayed,
          onRateStarClick: this.props.onRateStarClick,
          isRatingDisabled: this.props.disableRating,
          onVolumeBarChange: this.onVolumeBarChange,
          playerVolume: this.state.playerVolume
        }),
        _react2.default.createElement(_TrackList2.default, {
          tracksData: this.props.tracks,
          onItemClick: this.changeTrack,
          theme: theme
        })
      );
    }
  }]);

  return PlayerComponent;
}(_react2.default.Component);

PlayerComponent.defaultProps = {
  autoPlay: false,
  theme: 'violet',
  disableRating: false,
  onTrackChange: function onTrackChange() {},
  onTrackPlayed: function onTrackPlayed() {}
};
PlayerComponent.propTypes = {
  tracks: _propTypes2.default.arrayOf(_propTypes2.default.shape(_shapes.track)).isRequired,
  autoPlay: _propTypes2.default.bool,
  theme: _propTypes2.default.string,
  onRateStarClick: _propTypes2.default.func.isRequired,
  disableRating: _propTypes2.default.bool,
  onTrackChange: _propTypes2.default.func,
  onTrackPlayed: _propTypes2.default.func,
  activeTrackIndex: _propTypes2.default.number
};
exports.default = PlayerComponent;