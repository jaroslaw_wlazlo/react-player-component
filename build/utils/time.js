"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var convertSecondsToHumanTime = exports.convertSecondsToHumanTime = function convertSecondsToHumanTime(secondsCount) {
  var hours = Math.floor(secondsCount / 3600);
  var minutes = Math.floor((secondsCount - hours * 3600) / 60);
  var seconds = secondsCount - hours * 3600 - minutes * 60;

  var omitHours = hours === 0;

  hours = hours < 10 ? "0" + hours : hours;
  minutes = minutes < 10 ? "0" + minutes : minutes;
  seconds = seconds < 10 ? "0" + seconds : seconds;

  return omitHours ? minutes + ":" + seconds : hours + ":" + minutes + ":" + seconds;
};