'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var base = '';

var BEM = function () {
  function BEM(blockName) {
    var useBase = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

    _classCallCheck(this, BEM);

    this.blockName = useBase ? this.base === '' ? blockName : this.base + '-' + blockName : blockName;
  }

  _createClass(BEM, [{
    key: 'getBEMClassName',
    value: function getBEMClassName(suffix, separator) {
      return '' + this.blockName + separator + suffix;
    }
  }, {
    key: 'mod',
    value: function mod(_mod) {
      if (typeof _mod === 'string') {
        return this.getBEMClassName(_mod, '--');
      }
      var onlyKey = Object.keys(_mod)[0];
      return _mod[onlyKey] ? this.getBEMClassName(onlyKey, '--') : '';
    }
  }, {
    key: 'el',
    value: function el(elementName) {
      return new BEM(this.getBEMClassName(elementName, '__'), false);
    }
  }, {
    key: 'withMod',
    value: function withMod(modData) {
      var mod = this.mod(modData);
      return mod === '' ? this.blockName : this.blockName + ' ' + mod;
    }
  }, {
    key: 'toString',
    value: function toString() {
      return this.blockName;
    }
  }, {
    key: 'base',
    get: function get() {
      return base;
    }
  }], [{
    key: 'base',
    set: function set(value) {
      base = value;
    }
  }]);

  return BEM;
}();

BEM.base = 'rpc';

exports.default = BEM;