'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.BEM = exports.timeUtils = undefined;

var _BEM = require('./BEM');

Object.defineProperty(exports, 'BEM', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_BEM).default;
  }
});

var _time = require('./time');

var timeUtils = _interopRequireWildcard(_time);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.timeUtils = timeUtils;