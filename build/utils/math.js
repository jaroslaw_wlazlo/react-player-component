"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var getRandomIntInRange = exports.getRandomIntInRange = function getRandomIntInRange(min, max) {
  return parseInt(Math.random() * (max - min + 1) + min, 10);
};
var roundToNearestHalf = exports.roundToNearestHalf = function roundToNearestHalf(number) {
  return Math.round(number * 2) / 2;
};