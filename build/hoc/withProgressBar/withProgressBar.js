'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var getPercentage = function getPercentage(current, max) {
  var _ref = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
      asInt = _ref.asInt;

  if (max === 0) {
    return 0;
  }
  var percentage = current / max * 100;
  return asInt ? parseInt(percentage, 10) : percentage;
};

// TODO: remove asInt in some functions after https://github.com/facebook/react/issues/11532 is resolved
var withProgressBar = function withProgressBar(Component) {
  var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'horizontal';

  var ProgressBar = function (_React$PureComponent) {
    _inherits(ProgressBar, _React$PureComponent);

    function ProgressBar() {
      var _ref2;

      var _temp, _this, _ret;

      _classCallCheck(this, ProgressBar);

      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref2 = ProgressBar.__proto__ || Object.getPrototypeOf(ProgressBar)).call.apply(_ref2, [this].concat(args))), _this), _this.state = {
        barOffsetLeft: 0,
        barOffsetTop: 0,
        barWidth: 0,
        barHeight: 0,
        isIndicatorHold: false,
        indicatorPercentagePosition: 0
      }, _this.setIndicatorHoldValue = function () {
        _this.setState({
          isIndicatorHold: true
        });
      }, _this.handleHorizontalBarClick = function (e) {
        var relativeOffsetLeft = e.clientX - _this.state.barOffsetLeft;
        var newPercentagePosition = getPercentage(relativeOffsetLeft, _this.state.barWidth, { asInt: true });

        _this.setState({
          indicatorPercentagePosition: newPercentagePosition
        });

        _this.props.onChange(newPercentagePosition);
      }, _this.handleVerticalBarClick = function (e) {
        var relativeOffsetTop = e.clientY - _this.state.barOffsetTop;

        /* Standard offset count is from top to bottom. More natural is from bottom to top,
         so I invert percentage by subtracting result from 100 */
        var newPercentagePosition = 100 - getPercentage(relativeOffsetTop, _this.state.barHeight, { asInt: true });

        _this.setState({
          indicatorPercentagePosition: newPercentagePosition
        });

        _this.props.onChange(newPercentagePosition);
      }, _this.unsetIndicatorHoldValue = function () {
        _this.setState({
          isIndicatorHold: false
        });
      }, _this.handleHorizontalBarMouseMove = function (e) {
        e.stopPropagation();
        var _this$state = _this.state,
            barWidth = _this$state.barWidth,
            barOffsetLeft = _this$state.barOffsetLeft,
            isIndicatorHold = _this$state.isIndicatorHold,
            indicatorPercentagePosition = _this$state.indicatorPercentagePosition;

        var relativeOffsetLeft = e.clientX - barOffsetLeft;
        var newPercentagePosition = getPercentage(relativeOffsetLeft, barWidth, { asInt: true });

        // try to set state as few times as possible. Update position only if indicator is hold
        var canPositionBeSet = isIndicatorHold && indicatorPercentagePosition !== newPercentagePosition && relativeOffsetLeft <= barWidth && relativeOffsetLeft >= 0;
        if (canPositionBeSet) {
          _this.setState({
            indicatorPercentagePosition: newPercentagePosition
          });
          _this.props.onChange(newPercentagePosition);
        }
      }, _this.handleVerticalBarMouseMove = function (e) {
        e.stopPropagation();
        var _this$state2 = _this.state,
            barHeight = _this$state2.barHeight,
            barOffsetTop = _this$state2.barOffsetTop,
            isIndicatorHold = _this$state2.isIndicatorHold,
            indicatorPercentagePosition = _this$state2.indicatorPercentagePosition;

        var relativeOffsetTop = e.clientY - barOffsetTop;

        /* Standard offset count is from top to bottom. More natural is from bottom to top,
         so I invert percentage by subtracting result from 100 */
        var newPercentagePosition = 100 - getPercentage(relativeOffsetTop, barHeight, { asInt: true });

        // try to set state as few times as possible. Update position only if indicator is hold
        var canPositionBeSet = isIndicatorHold && indicatorPercentagePosition !== newPercentagePosition && relativeOffsetTop <= barHeight && relativeOffsetTop >= 0;
        if (canPositionBeSet) {
          _this.setState({
            indicatorPercentagePosition: newPercentagePosition
          });
          _this.props.onChange(newPercentagePosition);
        }
      }, _this.stopIndicatorClickPropagation = function (e) {
        e.stopPropagation();
      }, _this.progressWrapperRef = function (divElement) {
        _this.progressWrapperDOMElement = divElement;
      }, _temp), _possibleConstructorReturn(_this, _ret);
    }

    _createClass(ProgressBar, [{
      key: 'componentWillMount',
      value: function componentWillMount() {
        this.setState({
          indicatorPercentagePosition: getPercentage(this.props.current, this.props.max, { asInt: true })
        });
      }

      /* eslint-disable react/no-did-mount-set-state */
      // There's no refs before mount

    }, {
      key: 'componentDidMount',
      value: function componentDidMount() {
        var _progressWrapperDOMEl = this.progressWrapperDOMElement.getBoundingClientRect(),
            left = _progressWrapperDOMEl.left,
            top = _progressWrapperDOMEl.top,
            width = _progressWrapperDOMEl.width,
            height = _progressWrapperDOMEl.height;

        this.setState({
          barWidth: width,
          barHeight: height,
          barOffsetLeft: left,
          barOffsetTop: top
        });
      }

      /* eslint-disable react/no-did-mount-set-state */

    }, {
      key: 'componentWillReceiveProps',
      value: function componentWillReceiveProps(_ref3) {
        var nextCurrent = _ref3.current;
        var indicatorPercentagePosition = this.state.indicatorPercentagePosition;

        var nextIndicatorLeftPosition = getPercentage(nextCurrent, this.props.max, { asInt: true });

        if (indicatorPercentagePosition !== nextIndicatorLeftPosition) {
          this.setState({
            indicatorPercentagePosition: nextIndicatorLeftPosition
          });
        }
      }
    }, {
      key: 'render',
      value: function render() {
        var _props = this.props,
            onChange = _props.onChange,
            current = _props.current,
            max = _props.max,
            props = _objectWithoutProperties(_props, ['onChange', 'current', 'max']);

        var isHorizontal = type === 'horizontal';
        var onBarClick = isHorizontal ? this.handleHorizontalBarClick : this.handleVerticalBarClick;
        var onMouseMove = isHorizontal ? this.handleHorizontalBarMouseMove : this.handleVerticalBarMouseMove;
        return _react2.default.createElement(Component, _extends({
          onBarClick: onBarClick,
          onBarMouseMove: onMouseMove,
          progressWrapperRef: this.progressWrapperRef,
          setMouseHold: this.setIndicatorHoldValue,
          unsetMouseHold: this.unsetIndicatorHoldValue,
          onIndicatorClick: this.stopIndicatorClickPropagation,
          indicatorPercentagePosition: this.state.indicatorPercentagePosition,
          isIndicatorHold: this.state.isIndicatorHold
        }, props));
      }
    }]);

    return ProgressBar;
  }(_react2.default.PureComponent);

  ProgressBar.propTypes = {
    current: _propTypes2.default.number.isRequired,
    max: _propTypes2.default.number.isRequired,
    onChange: _propTypes2.default.func
  };
  ProgressBar.defaultProps = {
    onChange: function onChange() {}
  };


  return ProgressBar;
};

exports.default = withProgressBar;