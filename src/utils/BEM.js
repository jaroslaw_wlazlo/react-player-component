let base = '';

class BEM {
  constructor(blockName, useBase = true) {
    this.blockName = useBase ? this.base === '' ? blockName : `${this.base}-${blockName}` : blockName;
  }

  get base() {
    return base;
  }

  static set base(value) {
    base = value;
  }

  getBEMClassName(suffix, separator) {
    return `${this.blockName}${separator}${suffix}`;
  }

  mod(mod) {
    if (typeof mod === 'string') {
      return this.getBEMClassName(mod, '--');
    }
    const onlyKey = Object.keys(mod)[0];
    return mod[onlyKey] ? this.getBEMClassName(onlyKey, '--') : '';
  }

  el(elementName) {
    return new BEM(this.getBEMClassName(elementName, '__'), false);
  }

  withMod(modData) {
    const mod = this.mod(modData);
    return mod === '' ? this.blockName : `${this.blockName} ${mod}`;
  }

  toString() {
    return this.blockName;
  }
}

BEM.base = 'rpc';

export default BEM;
