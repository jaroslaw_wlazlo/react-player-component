export const convertSecondsToHumanTime = (secondsCount) => {
  let hours = Math.floor(secondsCount / 3600);
  let minutes = Math.floor((secondsCount - (hours * 3600)) / 60);
  let seconds = secondsCount - (hours * 3600) - (minutes * 60);

  const omitHours = hours === 0;

  hours = hours < 10 ? `0${hours}` : hours;
  minutes = minutes < 10 ? `0${minutes}` : minutes;
  seconds = seconds < 10 ? `0${seconds}` : seconds;

  return omitHours ? `${minutes}:${seconds}` : `${hours}:${minutes}:${seconds}`;
};
