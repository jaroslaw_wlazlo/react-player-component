export const getRandomIntInRange = (min, max) => parseInt(Math.random() * (max - min + 1) + min, 10);
export const roundToNearestHalf = number => Math.round(number * 2) / 2;
