import PropTypes from 'prop-types';

export default {
  trackUrl: PropTypes.string.isRequired,
  trackTitle: PropTypes.string.isRequired,
  coverUrl: PropTypes.string.isRequired,
  artistName: PropTypes.string.isRequired,
  rate: PropTypes.number,
  rateCount: PropTypes.number,
};
