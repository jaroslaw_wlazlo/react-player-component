import React from 'react';
import PropTypes from 'prop-types';

const getPercentage = (current, max, { asInt } = {}) => {
  if (max === 0) {
    return 0;
  }
  const percentage = (current / max) * 100;
  return asInt ? parseInt(percentage, 10) : percentage;
};

// TODO: remove asInt in some functions after https://github.com/facebook/react/issues/11532 is resolved
const withProgressBar = (Component, type = 'horizontal') => {
  class ProgressBar extends React.PureComponent {
    static propTypes = {
      current: PropTypes.number.isRequired,
      max: PropTypes.number.isRequired,
      onChange: PropTypes.func,
    };

    static defaultProps = {
      onChange: () => {},
    };

    state = {
      barOffsetLeft: 0,
      barOffsetTop: 0,
      barWidth: 0,
      barHeight: 0,
      isIndicatorHold: false,
      indicatorPercentagePosition: 0,
    };

    componentWillMount() {
      this.setState({
        indicatorPercentagePosition: getPercentage(this.props.current, this.props.max, { asInt: true }),
      });
    }

    /* eslint-disable react/no-did-mount-set-state */
    // There's no refs before mount
    componentDidMount() {
      const { left, top, width, height} = this.progressWrapperDOMElement.getBoundingClientRect();
      this.setState({
        barWidth: width,
        barHeight: height,
        barOffsetLeft: left,
        barOffsetTop: top,
      });
    }

    /* eslint-disable react/no-did-mount-set-state */

    componentWillReceiveProps({ current: nextCurrent }) {
      const { indicatorPercentagePosition } = this.state;
      const nextIndicatorLeftPosition = getPercentage(nextCurrent, this.props.max, { asInt: true });

      if (indicatorPercentagePosition !== nextIndicatorLeftPosition) {
        this.setState({
          indicatorPercentagePosition: nextIndicatorLeftPosition,
        });
      }
    }

    setIndicatorHoldValue = () => {
      this.setState({
        isIndicatorHold: true,
      });
    };

    handleHorizontalBarClick = (e) => {
      const relativeOffsetLeft = e.clientX - this.state.barOffsetLeft;
      const newPercentagePosition = getPercentage(relativeOffsetLeft, this.state.barWidth, { asInt: true });

      this.setState({
        indicatorPercentagePosition: newPercentagePosition,
      });

      this.props.onChange(newPercentagePosition);
    };

    handleVerticalBarClick = (e) => {
      const relativeOffsetTop = e.clientY - this.state.barOffsetTop;

      /* Standard offset count is from top to bottom. More natural is from bottom to top,
       so I invert percentage by subtracting result from 100 */
      const newPercentagePosition = 100 - getPercentage(relativeOffsetTop, this.state.barHeight, { asInt: true });

      this.setState({
        indicatorPercentagePosition: newPercentagePosition,
      });

      this.props.onChange(newPercentagePosition);
    };

    unsetIndicatorHoldValue = () => {
      this.setState({
        isIndicatorHold: false,
      });
    };

    handleHorizontalBarMouseMove = (e) => {
      e.stopPropagation();
      const { barWidth, barOffsetLeft, isIndicatorHold, indicatorPercentagePosition } = this.state;
      const relativeOffsetLeft = e.clientX - barOffsetLeft;
      const newPercentagePosition = getPercentage(relativeOffsetLeft, barWidth, { asInt: true });

      // try to set state as few times as possible. Update position only if indicator is hold
      const canPositionBeSet =
        isIndicatorHold
        && indicatorPercentagePosition !== newPercentagePosition
        && relativeOffsetLeft <= barWidth
        && relativeOffsetLeft >= 0;
      if (canPositionBeSet) {
        this.setState({
          indicatorPercentagePosition: newPercentagePosition,
        });
        this.props.onChange(newPercentagePosition);
      }
    };

    handleVerticalBarMouseMove = (e) => {
      e.stopPropagation();
      const { barHeight, barOffsetTop, isIndicatorHold, indicatorPercentagePosition } = this.state;
      const relativeOffsetTop = e.clientY - barOffsetTop;

      /* Standard offset count is from top to bottom. More natural is from bottom to top,
       so I invert percentage by subtracting result from 100 */
      const newPercentagePosition = 100 - getPercentage(relativeOffsetTop, barHeight, { asInt: true });

      // try to set state as few times as possible. Update position only if indicator is hold
      const canPositionBeSet =
        isIndicatorHold
        && indicatorPercentagePosition !== newPercentagePosition
        && relativeOffsetTop <= barHeight
        && relativeOffsetTop >= 0;
      if (canPositionBeSet) {
        this.setState({
          indicatorPercentagePosition: newPercentagePosition,
        });
        this.props.onChange(newPercentagePosition);
      }
    };

    stopIndicatorClickPropagation = (e) => {
      e.stopPropagation();
    };

    progressWrapperRef = (divElement) => {
      this.progressWrapperDOMElement = divElement;
    };

    progressWrapperDOMElement;

    render() {
      const { onChange, current, max, ...props } = this.props;
      const isHorizontal = type === 'horizontal';
      const onBarClick = isHorizontal ? this.handleHorizontalBarClick : this.handleVerticalBarClick;
      const onMouseMove = isHorizontal ? this.handleHorizontalBarMouseMove : this.handleVerticalBarMouseMove;
      return (
        <Component
          onBarClick={onBarClick}
          onBarMouseMove={onMouseMove}
          progressWrapperRef={this.progressWrapperRef}
          setMouseHold={this.setIndicatorHoldValue}
          unsetMouseHold={this.unsetIndicatorHoldValue}
          onIndicatorClick={this.stopIndicatorClickPropagation}
          indicatorPercentagePosition={this.state.indicatorPercentagePosition}
          isIndicatorHold={this.state.isIndicatorHold}
          {...props}
        />
      );
    }
  }

  return ProgressBar;
};

export default withProgressBar;
