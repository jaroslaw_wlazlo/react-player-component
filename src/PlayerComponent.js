import React from 'react';
import PropTypes from 'prop-types';

import { track as trackShape } from './shapes';
import Track from './components/Track';
import PlayerNavigation from './components/PlayerNavigation';
import TrackList from './components/TrackList';
import { getRandomIntInRange } from './utils/math';

// Want to keep style imports in one place
/* eslint-disable import/first */
import 'font-awesome/css/font-awesome.css';
/* eslint-disable import/first */
import './PlayerComponent.scss';

class PlayerComponent extends React.Component {
  static defaultProps = {
    autoPlay: false,
    theme: 'violet',
    disableRating: false,
    onTrackChange: () => {},
    onTrackPlayed: () => {},
  };

  static propTypes = {
    tracks: PropTypes.arrayOf(PropTypes.shape(trackShape)).isRequired,
    autoPlay: PropTypes.bool,
    theme: PropTypes.string,
    onRateStarClick: PropTypes.func.isRequired,
    disableRating: PropTypes.bool,
    onTrackChange: PropTypes.func,
    onTrackPlayed: PropTypes.func,
    activeTrackIndex: PropTypes.number,
  };

  state = {
    currentTrackIndex: 0,
    shouldLoop: false,
    isTrackPlayed: false,
    shouldShuffle: false,
    playerVolume: 1,
  };

  componentWillMount() {
    this.setState({
      isTrackPlayed: this.props.autoPlay,
      currentTrackIndex: this.props.activeTrackIndex || 0,
    });
  }

  componentWillReceiveProps({ activeTrackIndex: nextActiveTrackIndex }) {
    if (nextActiveTrackIndex && nextActiveTrackIndex !== this.state.currentTrackIndex) {
      this.changeTrack(nextActiveTrackIndex);
    }
  }

  onVolumeBarChange = (percentage) => {
    this.setState({
      playerVolume: percentage / 100,
    });
  };

  getNextIndex() {
    const { currentTrackIndex, shouldShuffle, shouldLoop } = this.state;
    const lastTrackIndex = this.props.tracks.length - 1;

    if (shouldLoop) {
      return currentTrackIndex;
    }

    if (shouldShuffle) {
      return getRandomIntInRange(0, lastTrackIndex);
    }

    return null;
  }

  goToPreviousTrack = () => {
    let nextIndex = this.getNextIndex();

    if (nextIndex === null) {
      const { currentTrackIndex } = this.state;
      nextIndex = currentTrackIndex > 0 ? currentTrackIndex - 1 : 0;
    }

    this.changeTrack(nextIndex);
  };

  goToNextTrack = () => {
    let nextIndex = this.getNextIndex();

    if (!nextIndex) {
      const { currentTrackIndex } = this.state;
      const lastTrackIndex = this.props.tracks.length - 1;
      nextIndex = currentTrackIndex < lastTrackIndex ? currentTrackIndex + 1 : lastTrackIndex;
    }

    this.changeTrack(nextIndex);
  };

  toggleTrackPlay = () => {
    this.setState({
      isTrackPlayed: !this.state.isTrackPlayed,
    });
  };

  togglePlayerLoop = () => {
    const { shouldLoop } = this.state;
    const toSet = {
      shouldLoop: !shouldLoop,
    };
    if (!shouldLoop) {
      toSet.shouldShuffle = false;
    }
    this.setState(toSet);
  };

  togglePlayerShuffle = () => {
    const { shouldShuffle } = this.state;
    const toSet = {
      shouldShuffle: !shouldShuffle,
    };
    if (!shouldShuffle) {
      toSet.shouldLoop = false;
    }
    this.setState(toSet);
  };

  changeTrack = (trackIndex) => {
    this.setState({
      currentTrackIndex: trackIndex,
    });
    this.props.onTrackChange(this.props.tracks[trackIndex]);
  };

  handleTrackEnd = () => {
    const nextIndex = this.getNextIndex();
    if (nextIndex !== null) {
      this.setState({
        currentTrackIndex: nextIndex,
      });
    }
    this.props.onTrackChange(this.props.tracks[nextIndex]);
  };

  render() {
    const { theme } = this.props;
    const { shouldLoop, currentTrackIndex, isTrackPlayed } = this.state;
    const currentTrack = this.props.tracks[currentTrackIndex];

    return (
      <div className="rpc-player">
        <Track
          theme={theme}
          trackData={currentTrack}
          shouldPlayTrack={isTrackPlayed}
          onTrackEnd={this.handleTrackEnd}
          onTrackPlay={this.props.onTrackPlayed}
          volume={this.state.playerVolume}
        />
        <PlayerNavigation
          theme={theme}
          isPlayerLooped={shouldLoop}
          isPlayerShuffled={this.state.shouldShuffle}
          trackData={currentTrack}
          onPlayButtonClick={this.toggleTrackPlay}
          onForwardButtonClick={this.goToNextTrack}
          onBackwardButtonClick={this.goToPreviousTrack}
          onShuffleButtonClick={this.togglePlayerShuffle}
          onLoopButtonClick={this.togglePlayerLoop}
          isTrackPlayed={isTrackPlayed}
          onRateStarClick={this.props.onRateStarClick}
          isRatingDisabled={this.props.disableRating}
          onVolumeBarChange={this.onVolumeBarChange}
          playerVolume={this.state.playerVolume}
        />
        <TrackList
          tracksData={this.props.tracks}
          onItemClick={this.changeTrack}
          theme={theme}
        />
      </div>
    );
  }
}

export default PlayerComponent;
