import React from 'react';
import PropTypes from 'prop-types';

import { track as trackShape } from '../../shapes';
import BEM from '../../utils/BEM';

import './TrackList.scss';

const b = new BEM('track-list');

const getTracks = (tracksData, onItemClick) =>
  tracksData.map((track, i) => (
    <li
      key={`${track.trackTitle}_${track.rate}_${i}`}
      className={b.el('item')}
      onClick={() => onItemClick(i)}
    >
      <span className={b.el('item-part')}>
        {track.trackTitle}
      </span>
      <span className={b.el('item-part')}>
        {track.artistName}
      </span>
    </li>
  ));

const TrackList = ({ tracksData, onItemClick, theme }) => (
  <div className={b.withMod(theme)}>
    <ul className={b.el('list')}>
      {getTracks(tracksData, onItemClick)}
    </ul>
  </div>
);

TrackList.propTypes = {
  tracksData: PropTypes.arrayOf(PropTypes.shape(trackShape)).isRequired,
  onItemClick: PropTypes.func.isRequired,
  theme: PropTypes.string.isRequired,
};

export default TrackList;
