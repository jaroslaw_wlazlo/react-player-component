import React from 'react';
import PropTypes from 'prop-types';
import ProgressBar from '../TimeBar';
import { timeUtils, BEM } from '../../utils';

import './Track.scss';

const b = new BEM('track');

class Track extends React.Component {
  static defaultProps = {
    shouldPlayTrack: false,
    volume: 1,
  };

  static propTypes = {
    theme: PropTypes.string.isRequired,
    trackData: PropTypes.shape({
      trackUrl: PropTypes.string.isRequired,
      trackTitle: PropTypes.string,
      coverUrl: PropTypes.string,
      artistName: PropTypes.string,
    }).isRequired,
    shouldPlayTrack: PropTypes.bool,
    onTrackEnd: PropTypes.func.isRequired,
    onTrackPlay: PropTypes.func.isRequired,
    volume: PropTypes.number,
  };

  state = {
    trackDuration: 0,
    currentTrackTime: 0,
  };

  componentDidMount() {
    this.audioDOMElement = document.createElement('audio');
    this.audioDOMElement.src = this.props.trackData.trackUrl;
    this.audioDOMElement.volume = this.props.volume;
    this.audioDOMElement.addEventListener('loadeddata', this.onAudioDataLoaded);
    this.audioDOMElement.addEventListener('timeupdate', this.onAudioTimeUpdate);
    this.audioDOMElement.addEventListener('ended', this.onAudioTrackEnd);
  }

  componentWillReceiveProps({ shouldPlayTrack, trackData, volume }) {
    const isTrackPaused = this.audioDOMElement.paused;

    if (this.audioDOMElement.src !== trackData.trackUrl) {
      this.audioDOMElement.src = trackData.trackUrl;
    }

    if (shouldPlayTrack && isTrackPaused) {
      this.audioDOMElement.play();
      this.props.onTrackPlay(this.props.trackData);
    } else if (!shouldPlayTrack && !isTrackPaused) {
      this.audioDOMElement.pause();
    }

    if (volume !== this.props.volume) {
      this.audioDOMElement.volume = volume;
    }
  }

  componentWillUnmount() {
    this.audioDOMElement.removeEventListener('loadeddata', this.onAudioDataLoaded);
    this.audioDOMElement.removeEventListener('timeupdate', this.onAudioTimeUpdate);
    this.audioDOMElement.removeEventListener('ended', this.onAudioTrackEnd);
    this.audioDOMElement.pause();
  }

  onAudioDataLoaded = ({ target: { duration, currentTime } }) => {
    this.setState({
      trackDuration: Math.floor(duration),
      currentTrackTime: currentTime,
    });
    if (this.props.shouldPlayTrack) {
      this.audioDOMElement.play();
      this.props.onTrackPlay(this.props.trackData);
    }
  };

  onAudioTrackEnd = () => {
    this.audioDOMElement.currentTime = 0;
    this.props.onTrackEnd();
  };

  onAudioTimeUpdate = (e) => {
    const { currentTrackTime } = this.state;
    const nextCurrentTime = Math.floor(e.target.currentTime);

    if (nextCurrentTime !== currentTrackTime) {
      this.setState({
        currentTrackTime: nextCurrentTime,
      });
    }
  };

  changeCurrentTrackTime = (percentage) => {
    this.audioDOMElement.currentTime = Math.floor((percentage / 100) * this.state.trackDuration);
  };

  audioDOMElement;

  render() {
    const { currentTrackTime, trackDuration } = this.state;
    return (
      <div className={b}>
        <div className={b.el('cover')}>
          <img className={b.el('cover-image')} src={this.props.trackData.coverUrl} alt="" />
        </div>
        <ProgressBar
          current={currentTrackTime}
          max={trackDuration}
          labelMin={timeUtils.convertSecondsToHumanTime(currentTrackTime)}
          labelMax={timeUtils.convertSecondsToHumanTime(trackDuration)}
          theme={this.props.theme}
          onChange={this.changeCurrentTrackTime}
        />
      </div>
    );
  }
}

export default Track;
