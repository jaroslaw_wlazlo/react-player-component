import React from 'react';
import PropTypes from 'prop-types';

const FontIcon = ({ icon, className, ...props }) => (
  <i className={`fa fa-${icon} ${className}`} {...props} />
);

FontIcon.propTypes = {
  icon: PropTypes.string.isRequired,
  className: PropTypes.string,
};

FontIcon.defaultProps = {
  className: '',
};

export default FontIcon;
