import React from 'react';
import PropTypes from 'prop-types';
import withProgressBar from '../../hoc/withProgressBar';
import BEM from '../../utils/BEM';

import './VolumeBar.scss';

const b = new BEM('volume-bar');

class VolumeBar extends React.PureComponent {
  static propTypes = {
    progressWrapperRef: PropTypes.func.isRequired,
    onBarClick: PropTypes.func.isRequired,
    onBarMouseMove: PropTypes.func.isRequired,
    setMouseHold: PropTypes.func.isRequired,
    unsetMouseHold: PropTypes.func.isRequired,
    onIndicatorClick: PropTypes.func.isRequired,
    indicatorPercentagePosition: PropTypes.number.isRequired,
    theme: PropTypes.string.isRequired,
  };

  render() {
    return (
      <div
        className={b.withMod(this.props.theme)}
        onMouseLeave={this.props.unsetMouseHold}
        onMouseUp={this.props.unsetMouseHold}
      >
        <div
          ref={this.props.progressWrapperRef}
          className={b.el('line')}
          onClick={this.props.onBarClick}
          onMouseMove={this.props.onBarMouseMove}
        >
          <div
            className={b.el('indicator')}
            onMouseDown={this.props.setMouseHold}
            onMouseUp={this.props.unsetMouseHold}
            onClick={this.props.onIndicatorClick}
            style={{
              bottom: `${this.props.indicatorPercentagePosition - 5}%`, // make it display in the middle of pointer
            }}
          />
        </div>
      </div>
    );
  }
}

export default withProgressBar(VolumeBar, 'vertical');
