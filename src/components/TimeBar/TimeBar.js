import React from 'react';
import PropTypes from 'prop-types';
import withProgressBar from '../../hoc/withProgressBar';
import BEM from '../../utils/BEM';

import './TimeBar.scss';

const b = new BEM('time-bar');

class TimeBar extends React.PureComponent {
  static propTypes = {
    progressWrapperRef: PropTypes.func.isRequired,
    onBarClick: PropTypes.func.isRequired,
    onBarMouseMove: PropTypes.func.isRequired,
    setMouseHold: PropTypes.func.isRequired,
    unsetMouseHold: PropTypes.func.isRequired,
    onIndicatorClick: PropTypes.func.isRequired,
    indicatorPercentagePosition: PropTypes.number.isRequired,
    labelMin: PropTypes.string.isRequired,
    labelMax: PropTypes.string.isRequired,
    theme: PropTypes.string.isRequired,
  };

  render() {
    return (
      <div
        className={b.withMod(this.props.theme)}
        onMouseLeave={this.props.unsetMouseHold}
        onMouseUp={this.props.unsetMouseHold}
      >
        <div className={b.el('label').withMod('min')}>
          {this.props.labelMin}
        </div>
        <div
          ref={this.props.progressWrapperRef}
          className={b.el('line')}
          onClick={this.props.onBarClick}
          onMouseMove={this.props.onBarMouseMove}
        >
          <div
            className={b.el('indicator')}
            onMouseDown={this.props.setMouseHold}
            onMouseUp={this.props.unsetMouseHold}
            onClick={this.props.onIndicatorClick}
            style={{
              left: `${this.props.indicatorPercentagePosition - 5}%`, // make it display in the middle of pointer
            }}
          />
        </div>
        <div className={b.el('label').withMod('max')}>
          {this.props.labelMax}
        </div>
      </div>
    );
  }
}

export default withProgressBar(TimeBar);
