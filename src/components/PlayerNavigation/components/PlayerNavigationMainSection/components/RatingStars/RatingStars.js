import React from 'react';
import PropTypes from 'prop-types';
import FontIcon from '../../../../../FontIcon';
import { roundToNearestHalf } from '../../../../../../utils/math';
import BEM from '../../../../../../utils/BEM';

import './RatingStars.scss';

const b = new BEM('rating-stars');

const RATE_STEP = 0.5;

class RatingStar extends React.Component {
  static propTypes = {
    rate: PropTypes.number.isRequired,
    rateCount: PropTypes.number.isRequired,
    onStarClick: PropTypes.func.isRequired,
  };

  state = {
    hoveredStarIndex: null,
  };

  getStarIconByIndex = (index) => {
    const { hoveredStarIndex } = this.state;
    const isAnyStarHovered = hoveredStarIndex !== null;

    if (isAnyStarHovered) {
      return index <= hoveredStarIndex ? 'star' : 'star-o';
    }

    const roundedRate = roundToNearestHalf(this.props.rate);
    if (roundedRate === index + RATE_STEP) {
      return 'star-half-o';
    } else if (roundedRate >= index + 1) {
      return 'star';
    }

    return 'star-o';
  };

  starOnMouseLeave = (e) => {
    e.stopPropagation();
    this.setState({
      hoveredStarIndex: null,
    });
  };

  createStarOnMouseOver = index => (e) => {
    e.stopPropagation();
    this.setState({
      hoveredStarIndex: index,
    });
  };

  // separate onClicks are created here to prevent recreating every render
  /* eslint-disable react/sort-comp */
  onFirstStarClick = () => {
    this.props.onStarClick(1);
  };
  onSecondStarClick = () => {
    this.props.onStarClick(2);
  };
  onThirdStarClick = () => {
    this.props.onStarClick(3);
  };
  onFourthStarClick = () => {
    this.props.onStarClick(4);
  };
  onFifthStarClick = () => {
    this.props.onStarClick(5);
  };
  /* eslint-disable react/sort-comp */

  render() {
    const starClassName = b.el('star');
    return (
      <div className={b}>
        <FontIcon
          className={starClassName}
          icon={this.getStarIconByIndex(0)}
          onMouseOver={this.createStarOnMouseOver(0)}
          onMouseLeave={this.starOnMouseLeave}
          onClick={this.onFirstStarClick}
        />
        <FontIcon
          className={starClassName}
          icon={this.getStarIconByIndex(1)}
          onMouseOver={this.createStarOnMouseOver(1)}
          onMouseLeave={this.starOnMouseLeave}
          onClick={this.onSecondStarClick}
        />
        <FontIcon
          className={starClassName}
          icon={this.getStarIconByIndex(2)}
          onMouseOver={this.createStarOnMouseOver(2)}
          onMouseLeave={this.starOnMouseLeave}
          onClick={this.onThirdStarClick}
        />
        <FontIcon
          className={starClassName}
          icon={this.getStarIconByIndex(3)}
          onMouseOver={this.createStarOnMouseOver(3)}
          onMouseLeave={this.starOnMouseLeave}
          onClick={this.onFourthStarClick}
        />
        <FontIcon
          className={starClassName}
          icon={this.getStarIconByIndex(4)}
          onMouseOver={this.createStarOnMouseOver(4)}
          onMouseLeave={this.starOnMouseLeave}
          onClick={this.onFifthStarClick}
        />
        <div className={b.el('count')}>
          ( {this.props.rateCount} )
        </div>
      </div>
    );
  }
}

export default RatingStar;
