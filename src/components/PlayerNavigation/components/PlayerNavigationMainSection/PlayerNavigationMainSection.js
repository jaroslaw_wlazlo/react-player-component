import React from 'react';
import PropTypes from 'prop-types';
import RatingStars from './components/RatingStars';
import { track as trackShape } from '../../../../shapes';
import BEM from '../../../../utils/BEM';

import './PlayerNavigationMainSection.scss';

const b = new BEM('player-navigation-main-section');

const PlayerNavigationMainSection = ({ theme, trackData, onRateStarClick, isRatingDisabled }) => (
  <div className={b.withMod(theme)}>
    <h3 className={b.el('artist-name')}>
      {trackData.artistName}
    </h3>
    <h4 className={b.el('song-title')}>
      {trackData.trackTitle}
    </h4>
    {!isRatingDisabled && (
      <div className={b.el('rating')}>
        <RatingStars
          rate={trackData.rate}
          rateCount={trackData.rateCount}
          onStarClick={rate => onRateStarClick(rate, trackData)}
        />
      </div>
    )}
  </div>
);

PlayerNavigationMainSection.propTypes = {
  trackData: PropTypes.shape(trackShape).isRequired,
  theme: PropTypes.string.isRequired,
  onRateStarClick: PropTypes.func.isRequired,
  isRatingDisabled: PropTypes.bool.isRequired,
};

export default PlayerNavigationMainSection;
