import React from 'react';
import PropTypes from 'prop-types';
import FontIcon from '../../../FontIcon';
import VolumeBar from '../../../VolumeBar';
import BEM from '../../../../utils/BEM';

import './PlayerNavigationFooter.scss';

const PLAYER_VOLUME_MAX = 1;

const b = new BEM('player-navigation-footer');

class PlayerNavigationFooter extends React.Component {
  static propTypes = {
    theme: PropTypes.string.isRequired,
    playButtonIcon: PropTypes.string.isRequired,
    onPlayButtonClick: PropTypes.func.isRequired,
    onForwardButtonClick: PropTypes.func.isRequired,
    onBackwardButtonClick: PropTypes.func.isRequired,
    onShuffleButtonClick: PropTypes.func.isRequired,
    onVolumeBarChange: PropTypes.func.isRequired,
    isPlayerShuffled: PropTypes.bool.isRequired,
    playerVolume: PropTypes.number.isRequired,
  };

  state = {
    isVolumeBarActive: false,
  };

  getVolumeIcon() {
    const { playerVolume } = this.props;

    if (playerVolume === 0) {
      return 'volume-off';
    } else if (playerVolume <= PLAYER_VOLUME_MAX / 2 && playerVolume !== PLAYER_VOLUME_MAX - 0.05) {
      return 'volume-down';
    }

    return 'volume-up';
  }

  toggleVolumeBarActive = () => {
    this.setState({
      isVolumeBarActive: !this.state.isVolumeBarActive,
    });
  };

  render() {
    const { playerVolume } = this.props;
    // let the volume icon show as active a little bit earlier, give it error border of 0.05
    return (
      <div className={b.withMod(this.props.theme)}>
        <div className={b.el('volume')}>
          <FontIcon
            className={b.el('icon').withMod({ active: playerVolume >= PLAYER_VOLUME_MAX - 0.05 })}
            icon={this.getVolumeIcon()}
            onClick={this.toggleVolumeBarActive}
          />
          {this.state.isVolumeBarActive && (
            <VolumeBar
              current={playerVolume}
              max={1}
              theme={this.props.theme}
              onChange={this.props.onVolumeBarChange}
            />
          )}
        </div>
        <FontIcon
          className={b.el('icon').withMod('backward')}
          icon="fast-backward"
          onClick={this.props.onBackwardButtonClick}
        />
        <FontIcon
          className={b.el('icon').withMod('active')}
          icon={this.props.playButtonIcon}
          onClick={this.props.onPlayButtonClick}
        />
        <FontIcon
          className={b.el('icon').withMod('forward')}
          icon="fast-forward"
          onClick={this.props.onForwardButtonClick}
        />
        <FontIcon
          className={b.el('icon').withMod({ active: this.props.isPlayerShuffled })}
          icon="random"
          onClick={this.props.onShuffleButtonClick}
        />
      </div>
    );
  }
}

export default PlayerNavigationFooter;
