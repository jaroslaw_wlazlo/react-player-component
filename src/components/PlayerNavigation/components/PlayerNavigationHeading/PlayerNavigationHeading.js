import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import FontIcon from '../../../FontIcon';
import BEM from '../../../../utils/BEM';

import './PlayerNavigationHeading.scss';

const b = new BEM('player-navigation-heading');

class PlayerNavigationHeading extends React.Component {
  static propTypes = {
    isTrackPlayed: PropTypes.bool,
    isPlayerLooped: PropTypes.bool,
    isFavouritesActive: PropTypes.bool,
    isAddedToFavourites: PropTypes.bool,
    onLoopButtonClick: PropTypes.func.isRequired,
    toggleAddedToFavouritesActive: PropTypes.func,
    theme: PropTypes.string.isRequired,
  };

  static defaultProps = {
    isAddedToFavourites: false,
    isPlayerLooped: false,
    isTrackPlayed: false,
    isFavouritesActive: false,
    toggleAddedToFavouritesActive: null,
  };

  render() {
    return (
      <div className={b.withMod(this.props.theme)}>
        <FontIcon
          className={b.el('icon').withMod({ active: this.props.isPlayerLooped })}
          icon="repeat"
          title="repeat"
          onClick={this.props.onLoopButtonClick}
        />
        <h2 className={b.el('text')}>
          {this.props.isTrackPlayed
            ? 'odtwarzanie...'
            : 'odwtórz ten utwór'
          }
        </h2>
        <FontIcon
          className={b.el('icon').withMod({ active: this.props.isAddedToFavourites })}
          icon="heart"
          title="add to favourites"
          onClick={this.props.isFavouritesActive && this.props.toggleAddedToFavouritesActive}
        />
      </div>
    );
  }
}

export default PlayerNavigationHeading;
