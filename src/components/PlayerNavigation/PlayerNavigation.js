import React from 'react';
import PropTypes from 'prop-types';

import { track as trackShape } from '../../shapes';
import PlayerNavigationHeading from './components/PlayerNavigationHeading';
import PlayerNavigationMainSection from './components/PlayerNavigationMainSection';
import PlayerNavigationFooter from './components/PlayerNavigationFooter';

import './PlayerNavigation.scss';

const PlayerNavigation = ({ theme, ...props }) => (
  <div className="rpc-player-navigation">
    <PlayerNavigationHeading
      theme={theme}
      isTrackPlayed={props.isTrackPlayed}
      isFavouritesActive={props.isFavouritesActive}
      isPlayerLooped={props.isPlayerLooped}
      onLoopButtonClick={props.onLoopButtonClick}
    />
    <PlayerNavigationMainSection
      theme={theme}
      trackData={props.trackData}
      onRateStarClick={props.onRateStarClick}
      isRatingDisabled={props.isRatingDisabled}
    />
    <PlayerNavigationFooter
      theme={theme}
      playButtonIcon={props.isTrackPlayed ? 'pause' : 'play'}
      onPlayButtonClick={props.onPlayButtonClick}
      onForwardButtonClick={props.onForwardButtonClick}
      onBackwardButtonClick={props.onBackwardButtonClick}
      onShuffleButtonClick={props.onShuffleButtonClick}
      onVolumeBarChange={props.onVolumeBarChange}
      isPlayerShuffled={props.isPlayerShuffled}
      playerVolume={props.playerVolume}
    />
  </div>
);

PlayerNavigation.propTypes = {
  theme: PropTypes.string.isRequired,
  isFavouritesActive: PropTypes.bool,
  isPlayerLooped: PropTypes.bool,
  isPlayerShuffled: PropTypes.bool,
  isTrackPlayed: PropTypes.bool,
  onPlayButtonClick: PropTypes.func.isRequired,
  onForwardButtonClick: PropTypes.func.isRequired,
  onBackwardButtonClick: PropTypes.func.isRequired,
  onShuffleButtonClick: PropTypes.func.isRequired,
  onLoopButtonClick: PropTypes.func.isRequired,
  trackData: PropTypes.shape(trackShape).isRequired,
  onRateStarClick: PropTypes.func.isRequired,
  onVolumeBarChange: PropTypes.func.isRequired,
  isRatingDisabled: PropTypes.bool,
  playerVolume: PropTypes.number.isRequired,
};

PlayerNavigation.defaultProps = {
  isFavouritesActive: false,
  isPlayerLooped: false,
  isPlayerShuffled: false,
  isTrackPlayed: false,
  isRatingDisabled: false,
};

export default PlayerNavigation;
