import React from 'react';
/* eslint-disable import/no-extraneous-dependencies */
import ReactDOM from 'react-dom';
/* eslint-disable import/no-extraneous-dependencies */
import PlayerComponent from './src';

const playbackData = [
  {
    trackUrl: 'http://tegos.kz/new/mp3_full/Redfoo_-_New_Thang.mp3',
    trackTitle: 'Title',
    coverUrl: 'https://www.reduceimages.com/img/image-after.jpg',
    artistName: 'John Snow',
    rate: 4.5,
    rateCount: 21,
  },
  {
    trackUrl: 'https://zespolkropkanadi.github.io/Marina%20mp3.mp3',
    trackTitle: 'Marina',
    coverUrl: 'https://www.reduceimages.com/img/image-after.jpg',
    artistName: 'Cover Kropka nad i',
    rate: 4.5,
    rateCount: 21,
  },
];

/* eslint-disable */
ReactDOM.render(
  <PlayerComponent
    tracks={playbackData}
    onRateStarClick={(index) => { alert(`Rate: ${index}`); }}
    theme="coffee"
    activeTrackIndex={1}
  />,
  document.getElementById('demo'),
);
/* eslint-disable no-undef */
